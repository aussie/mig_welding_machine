#include <Arduino.h>
#include <TM1637Display.h> // the TM1637 arduino library

// Display connection pins (Digital Pins)
#define D_CLK 3
#define D_DIO 4

#define DEBOUNCE_DELAY 100
#define LONG_PRESS_DELAY 2000

#define CO2_START_DELAY 500
#define CO2_POST_FLOW_DELAY 1000

#define TOURCH_TRIGGER_PIN 13
#define CO2_VALVE_PIN 11
#define MOTOR_PIN 9
#define MOTOR_BRAKE_PIN 2
#define POWER_PIN 12

#define BTN_NO_GAS_PIN 1
#define BTN_2T_4T_PIN 0
#define LED_2T_4T_PIN 5
#define LED_NO_GAS_PIN 6

#define MIN_MOTOR_SPEED 32
#define MAX_MOTOR_SPEED 320

// BURN_BACK - annealing the wire at the end of the welding session
#define BURN_BACK_TIME 300
// RUN-IN - for a smooth increase in the wire feed speed. Provides clean and smooth arc ignition 
#define RUN_IN_VALUE 5
#define RUN_IN_STEP 1
#define RUN_IN_STEP_DELAY 100

#define DEBUG

TM1637Display display(D_CLK, D_DIO);

void setup() {
#ifdef DEBUG
  Serial.begin(9600);
  Serial.println("Start");
#endif

  display.setBrightness(220);

  // set pin mode for CO2 valve as outpput
  pinMode(CO2_VALVE_PIN, OUTPUT);
  // set pin mode for POWER signal which will enable the welding machine
  pinMode(POWER_PIN, OUTPUT);
  // will stop the motor after trigger release
  pinMode(MOTOR_BRAKE_PIN, OUTPUT);
  // set pin mode for tourch trigger as input
  pinMode(TOURCH_TRIGGER_PIN, OUTPUT);
  // Set 2T/4T mode button pin
  pinMode(BTN_2T_4T_PIN, INPUT);
  // Set 2T/4T mode LED pin
  pinMode(LED_2T_4T_PIN, OUTPUT);
  // Set the PWM pins as output.
  pinMode(MOTOR_PIN, OUTPUT);
  // Set NO-GAS mode button pin
  pinMode(BTN_NO_GAS_PIN, INPUT);
  // Set NO-GAS mode LED pin
  pinMode(LED_NO_GAS_PIN, OUTPUT);

  // pull tourch trigger to 5v
  digitalWrite(TOURCH_TRIGGER_PIN, 1);
  // pull 2T/4T button to 5v
  digitalWrite(BTN_2T_4T_PIN, 1);
  // pull NO_GAS button to 5v
  digitalWrite(BTN_NO_GAS_PIN, 1);
  // Actually don't need that, just to be sure. We don't want short 
  digitalWrite(MOTOR_BRAKE_PIN, 0);
  // initially power is OFF
  digitalWrite(POWER_PIN, 0);
  // pinMode(A0, INPUT);
  // digitalWrite(A0, 0);
  
  // display mode as 2T
  digitalWrite(LED_2T_4T_PIN, 0);

  // CO2 valve off
  digitalWrite(CO2_VALVE_PIN, 0);

  // Configure Timer 1 for PWM @ 25 kHz.
  TCCR1A = 0;             // undo the configuration done by...
  TCNT1 = 0;              // reset timer
  TCCR1A = _BV(COM1A1)    // non-inverted PWM on ch. A
           | _BV(WGM11);  // mode 10: ph. correct PWM, TOP = ICR1
  TCCR1B = _BV(WGM13)     // ditto
           | _BV(CS10);   // prescaler = 1
  ICR1 = MAX_MOTOR_SPEED; // TOP = 320
}

enum BURN_MODE {
  IDLE_MODE = 0,
  INIT = 1,
  WELD_INIT = 2,
  WELD = 3,
  STOP = 4,
  CO2_POST_FLOW = 5
}; 

enum TRIGGER_MODE {
  IDLE = 0,
  WELDING = 1,
  CONTINUE_4T = 2,
  STOP_4T = 3
};

int currSpeed = 0;
int speedVal = 0;
int runInSpeed = 0;
int runInDelay = 0;
int is4TMode = 0;
int isNoGASMode = 0;

int CO2_Delay = 0;
int isCO2PostFlowOpened = 0;

int isShowSpeedVal = 1;

BURN_MODE burnMode = IDLE_MODE;
TRIGGER_MODE triggerMode = IDLE;

void setSpeed(int speed) {
  currSpeed = speed;
  OCR1A = speed;
}

void inline readSpeedValue() {
  uint16_t speedRawVal = analogRead(A0);
  speedVal = map(speedRawVal, 0, 1024, MIN_MOTOR_SPEED, MAX_MOTOR_SPEED);

  // display current speed in percents (0 - 100). TODO: recalculate current speed to M/min
  currSpeed = map(speedRawVal, 0, 1024, 0, 100);

  if(isShowSpeedVal) {
    display.showNumberDec(currSpeed, true, 4, 0);
  }   
}

void inline btn2T_4THandler() {
  // read 2T/4T button state
  if (!digitalRead(BTN_2T_4T_PIN)) {
    delay(DEBOUNCE_DELAY);
    if (!digitalRead(BTN_2T_4T_PIN)) {
      is4TMode = is4TMode ? 0 : 1;
      digitalWrite(LED_2T_4T_PIN, is4TMode);
    }
  }
}

void inline btnNoGasHandler() {
  // read NO-GAS button state
  if(!digitalRead(BTN_NO_GAS_PIN)) {
    delay(DEBOUNCE_DELAY);
    uint64_t BTN_Delay = millis();
    int isLongPressed = 0;
    
    while(!digitalRead(BTN_NO_GAS_PIN)) {
      if(millis() - BTN_Delay >= LONG_PRESS_DELAY) {
        isLongPressed = 1;
        digitalWrite(CO2_VALVE_PIN, 1); // open CO2 valve
      }
    }

    if(isLongPressed) {
      digitalWrite(CO2_VALVE_PIN, 0); // close CO2 valve
    } else {
      isNoGASMode = isNoGASMode ? 0 : 1;
      digitalWrite(LED_NO_GAS_PIN, isNoGASMode);
    }
  }
}

/**
 * Cloce CO2 valve
 */
void disableCO2() {
  digitalWrite(CO2_VALVE_PIN, 0);
  isShowSpeedVal = 1;
  isCO2PostFlowOpened = 0;
  burnMode = BURN_MODE::IDLE_MODE; 
}

void loop() {
  // 2T/4T button
  btn2T_4THandler();

  // NO-GAS button
  btnNoGasHandler();

  // read wire speed value
  readSpeedValue();
      
  // read tourch trigger state
  int _triggerState = digitalRead(TOURCH_TRIGGER_PIN);
  delay(DEBOUNCE_DELAY);
  int triggerState = digitalRead(TOURCH_TRIGGER_PIN);

  if(_triggerState == triggerState) {
    switch (triggerMode) {
      case TRIGGER_MODE::IDLE:
        // trigger button pushed
        if(!triggerState) {
          triggerMode = TRIGGER_MODE::WELDING;
          burnMode = BURN_MODE::INIT;
        }
        break;
        
      case TRIGGER_MODE::WELDING:
        // trigger button released
        if (triggerState) {
          if(is4TMode) {
            // if 4T mode => continue welding and wait for the second triggering
            triggerMode = TRIGGER_MODE::CONTINUE_4T;
          } else {
            // stop welding
            triggerMode = TRIGGER_MODE::IDLE;
            burnMode = BURN_MODE::STOP;
          }
        }
        break;
        
      case TRIGGER_MODE::CONTINUE_4T: 
        // trigger button pushed
        if (!triggerState) {
          triggerMode = TRIGGER_MODE::STOP_4T;
        }
        break;
        
      case TRIGGER_MODE::STOP_4T:
        // trigger button released
        if (triggerState) {
          // 4T cycle is over
          triggerMode = TRIGGER_MODE::IDLE;
          // stop welding
          burnMode = BURN_MODE::STOP;
        }
        break;
    }
  }

  // welding proccess
  switch (burnMode) {
    case BURN_MODE::INIT:
      if(!isCO2PostFlowOpened) {
        digitalWrite(CO2_VALVE_PIN, 0);
      }
      burnMode = BURN_MODE::WELD_INIT;

      break;
    case BURN_MODE::WELD_INIT:
      // Enable the CO2 valve (if CO2 enabled)
      if(!isNoGASMode) {
        digitalWrite(CO2_VALVE_PIN, 1);
        // Wait for CO2 start delay
        if(!isCO2PostFlowOpened) {
          delay(CO2_START_DELAY);
        }
      }

      // Enable POWER.
      digitalWrite(POWER_PIN, 1);

      // Release motor break
      digitalWrite(MOTOR_BRAKE_PIN, 0); // release the motor brake
    
      // RunIn speed calculation
      runInDelay = millis();
      runInSpeed = speedVal - RUN_IN_VALUE;
      runInSpeed = runInSpeed > MIN_MOTOR_SPEED ? runInSpeed : speedVal;

      burnMode = BURN_MODE::WELD;
      
      break;
    case BURN_MODE::WELD:

      if(runInSpeed < speedVal) {
        if(millis() - runInDelay >= RUN_IN_STEP_DELAY) {
          runInDelay = millis();
          runInSpeed += RUN_IN_STEP;
        }   
      }
      
      isShowSpeedVal = 0;
      currSpeed = map(runInSpeed, 0, 1024, 0, 100);
      display.showNumberDec(currSpeed, true, 4, 0);

      setSpeed(runInSpeed); // feed the wire  
            
      break;
    case BURN_MODE::STOP:
      // Stop the wire feeding
      setSpeed(0);
      // stop the motor instantly
      digitalWrite(MOTOR_BRAKE_PIN, 1);
      // BURN-BACK delaying
      delay(BURN_BACK_TIME);
      // Disable POWER
      digitalWrite(POWER_PIN, 0);

      // Release motor break
      digitalWrite(MOTOR_BRAKE_PIN, 0);

      // Disable CO2 after delay
      if(!isNoGASMode) {
        burnMode = BURN_MODE::CO2_POST_FLOW;
        CO2_Delay = millis();
        isCO2PostFlowOpened = 1;
      } else {
        disableCO2();
      }
      
      break;

    case BURN_MODE::CO2_POST_FLOW:
      if(millis() - CO2_Delay >= CO2_POST_FLOW_DELAY) {
         disableCO2();
      }
     
      break;
  }
}